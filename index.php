<?php
/**
 * Created by PhpStorm.
 * User: ant45527
 * Date: 2/13/2017
 * Time: 9:38 AM
 */

spl_autoload_register('autoload');

// Autoloads classes under the php folder
function autoload( $class, $dir = null ) {
    if ($dir == null)
        $dir = getcwd() . "/php/";

    // Make class names lower case
    $class = strtolower($class);

    foreach (scandir($dir) as $file) {
        // If it's a directory
        if (is_dir( $dir.$file ) && substr( $file, 0, 1 ) !== '.')
            autoload( $class, $dir.$file.'/' );

        // If it's a .php file
        if (substr( $file, 0, 2 ) !== '._' && preg_match( "/.php$/i" , $file )) {
            // If it's a class
            if ((str_replace( '.class.php', '', $file )) == $class ) {
                include $dir . $file;
            }
        }
    }
}

// Build the HTML head
$head = "<head>
            <link rel='stylesheet' type='text/css' href='css/main.css'>
            <link rel='stylesheet' href='lobibox/dist/css/lobibox.min.css'/>
            <script type='text/javascript' src='js/jquery-latest.js'></script>
            <script src='lobibox/dist/js/lobibox.min.js'></script>
            <script type='text/javascript' src='js/main.js'></script>
            <title>WKU Parking Finder</title>
        </head>";

// Build the HTML body
$body = "<body>" . new Parking_Finder_View() ."</body>";

// Echo the HTML
echo $head . $body;
<?php
/**
 * Created by PhpStorm.
 * User: drew
 * Date: 4/30/17
 * Time: 9:38 PM
 */

// Filter the sorting type input
$closed = filter_input(INPUT_POST, 'closed', FILTER_SANITIZE_STRING);

// Start the session
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

// Update session for filtering
$_SESSION["filtering_closed"] = $closed;
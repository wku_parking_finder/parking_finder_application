<?php
/**
 * Created by PhpStorm.
 * User: drew
 * Date: 4/27/17
 * Time: 9:55 PM
 */

include_once("../php/parking_lot/parking_lot_floor.class.php");
include_once("../php/view/control_panel_view.class.php");
include_once("../php/sqler/sqler.class.php");

/// Start the session
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

// Create data array
$data = [];

// Check if we are admin or mod
if ($_SESSION["user_type"] == 1) {
    // Filter inputs
    $floorId = filter_input(INPUT_POST, 'floorId', FILTER_SANITIZE_NUMBER_INT);
    $status = filter_input(INPUT_POST, 'status', FILTER_SANITIZE_NUMBER_INT);

    // Do opposite of current state
    $status = ($status == 1 ? 0 : 1);

    // Get the floor from the database
    $floor = Parking_Lot_Floor::getByID($floorId);

    // Update the close property for the floors
    $floor->status = $status;

    // Save it
    $floor->save();

    // Get the new status img
    $data["newFloorStatus"] = Control_Panel_View::getFloorStatusIcon($floor);

    // Set the response success to true
    $data["success"] = TRUE;
}
else {
    // Set the response success to false
    $data["success"] = FALSE;
}


// Echo the encoded response
echo json_encode($data);
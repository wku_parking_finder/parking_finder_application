<?php
/**
 * Created by PhpStorm.
 * User: drew
 * Date: 2/23/17
 * Time: 9:25 PM
 */

include_once("../php/view/detailed_parking_lot_view.class.php");

// Filter inputs
$lotId = filter_input(INPUT_POST, 'lotId', FILTER_SANITIZE_NUMBER_INT);

// Create the data array
$data = [];

// Add the view to the array
$data["detailed_view"] = (new Detailed_Parking_Lot_View($lotId))->__toString();

// Echo the JSON encoded view
echo json_encode($data);
<?php
/**
 * Created by PhpStorm.
 * User: drew
 * Date: 4/5/17
 * Time: 10:49 AM
 */

include_once("../php/parking_lot/parking_lot_floor.class.php");
include_once("../php/sqler/sqler.class.php");
include_once("../php/view/control_panel_view.class.php");

// Filter inputs
$lotId = filter_input(INPUT_POST, 'lotId', FILTER_SANITIZE_NUMBER_INT);
$floorPermitRequirement = filter_input(INPUT_POST, 'floorPermitRequirement', FILTER_SANITIZE_NUMBER_INT);
$floorCapacity = filter_input(INPUT_POST, 'floorCapacity', FILTER_SANITIZE_STRING);
$floorHandicapCapacity = filter_input(INPUT_POST, 'floorHandicapCapacity', FILTER_SANITIZE_STRING);
$floorClosed = filter_input(INPUT_POST, 'floorClosed', FILTER_SANITIZE_NUMBER_INT);
$floorStatus = filter_input(INPUT_POST, 'floorStatus', FILTER_SANITIZE_NUMBER_INT);

// Start the session
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

// Create data array
$data = [];

// Check if we have a result
if ($_SESSION["user_type"] == 1) {
    // Create the new parking floor
    $parkingFloor = new Parking_Lot_Floor($lotId, $floorPermitRequirement, $floorCapacity,
        $floorHandicapCapacity, $floorClosed, $floorCapacity, $floorHandicapCapacity);
    // Save it and store the success
    $data["success"] = $parkingFloor->save();
    // Get the row to append
    $data["newFloorRow"] = Control_Panel_View::getParkingLotFloorRow($parkingFloor);
}
else {
    // Set the response success to false
    $data["success"] = false;
}


// Echo the encoded response
echo json_encode($data);
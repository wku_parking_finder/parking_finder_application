<?php
/**
 * Created by PhpStorm.
 * User: drew
 * Date: 3/27/17
 * Time: 9:46 AM
 */

// Start the session
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

// Create data array
$data = [];

if ( 0 < $_FILES['file']['error'] || $_FILES['file'] == NULL) {
    $data["success"] = FALSE;
    $data["message"] = 'Error: ' . $_FILES['file']['error'];
}
else {
    if ($_SESSION["user_type"] == 1) {
        $data["success"] = TRUE;
        $file = str_replace( "\\", '/', $_FILES['file']['name'] );
        move_uploaded_file($_FILES['file']['tmp_name'] , dirname( dirname(__FILE__) ) . '/uploads/' . $file);
        $data["new_image"] = $file;
    }
    else {
        $data["success"] = FALSE;
        $data["message"] = "Insufficient permissions.";
    }
}

// Echo the encoded response
echo json_encode($data);
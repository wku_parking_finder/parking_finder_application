<?php
/**
 * Created by PhpStorm.
 * User: drew
 * Date: 2/26/17
 * Time: 9:00 PM
 */

include_once("../php/view/control_panel_view.class.php");
include_once("../php/sqler/sqler.class.php");

// Filter inputs
$username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
$password = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING);

// Start the session
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

// Create data array
$data = [];

// Create the SQLer
$sqler = new SQLer();

// Hash the password
$hashedPass = $sqler->hashPass($password);

// Select
$sqler->sendQuery("Select user_type from users where user_id='$username' and password='$hashedPass'");

// Check if we have a result
if ($row = $sqler->getRow()) {
    // Set the response success to true
    $data["success"] = true;
    // Set the session id for the user
    $_SESSION["user_type"] = $row["user_type"];
    // Add the control panel view to the array
    $data["toAppend"] = (new Control_Panel_View())->__toString();
}
else {
    // Set the response success to false
    $data["success"] = false;
}


// Echo the encoded response
echo json_encode($data);

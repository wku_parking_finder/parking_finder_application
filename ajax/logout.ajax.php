<?php
/**
 * Created by PhpStorm.
 * User: drew
 * Date: 3/21/17
 * Time: 10:08 AM
 */

// Start the session
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

// Unset the session variable for the user type
$_SESSION["user_type"] = NULL;
<?php
/**
 * Created by PhpStorm.
 * User: drew
 * Date: 4/27/17
 * Time: 10:17 PM
 */

include_once("../php/view/parking_finder_view.class.php");
include_once("../php/parking_lot/parking_lot.class.php");
include_once("../php/parking_lot/parking_lot_floor.class.php");

// Create the data array
$data = ["success" => TRUE, "latestLotsTable" => Parking_Finder_View::getLotsTable()];

// Echo the json encoded array
echo json_encode($data);
<?php
/**
 * Created by PhpStorm.
 * User: drew
 * Date: 3/21/17
 * Time: 9:50 AM
 */

include_once("../php/view/create_parking_lot_form_view.class.php");

// Start the session
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

// Create data array
$data = [];

// Check for adequate privileges
if ($_SESSION["user_type"] == 1) {
    // Set the response success to true
    $data["success"] = true;
    // Add the control panel view to the array
    $data["toAppend"] = (new Create_Parking_Lot_Form_View())->__toString();
}
else {
    // Set the response success to false
    $data["success"] = false;
}


// Echo the encoded response
echo json_encode($data);
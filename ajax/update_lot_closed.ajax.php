<?php
/**
 * Created by PhpStorm.
 * User: drew
 * Date: 4/26/17
 * Time: 9:18 AM
 */

include_once("../php/parking_lot/parking_lot.class.php");
include_once("../php/parking_lot/parking_lot_floor.class.php");
include_once("../php/sqler/sqler.class.php");

// Start the session
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
// Create data array
$data = [];

// Check if we are admin or mod
if ($_SESSION["user_type"] == 1 || $_SESSION["user_type"] == 0) {
    // Filter inputs
    $lotId = filter_input(INPUT_POST, 'lotId', FILTER_SANITIZE_NUMBER_INT);
    $close = filter_input(INPUT_POST, 'close', FILTER_SANITIZE_NUMBER_INT);

    // Get the lot from the database
    $lot = Parking_Lot::getByID($lotId);

    // Update the close property for the floors
    $lot->toggleAllFloorClosings($close);

    // Update the close property for the lot
    $lot->closed = $close;

    // Save it
    $lot->save();

    $data["success"] = TRUE;
}
else {
    // Set the response success to false
    $data["success"] = FALSE;
}


// Echo the encoded response
echo json_encode($data);
<?php
/**
 * Created by PhpStorm.
 * User: drew
 * Date: 4/27/17
 * Time: 10:26 AM
 */

include_once("../php/parking_lot/parking_lot_floor.class.php");
include_once("../php/sqler/sqler.class.php");

// Start the session
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

// Create data array
$data = [];

// Check if we are admin or mod
if ($_SESSION["user_type"] == 1 || $_SESSION["user_type"] == 0) {
    // Filter inputs
    $floorId = filter_input(INPUT_POST, 'floorId', FILTER_SANITIZE_NUMBER_INT);
    $close = filter_input(INPUT_POST, 'close', FILTER_SANITIZE_NUMBER_INT);

    // Get the lot from the database
    $floor = Parking_Lot_Floor::getByID($floorId);

    // Update the close property for the floors
    $floor->closed = $close;

    // Save it
    $floor->save();

    // Set successful
    $data["success"] = TRUE;
}
else {
    // Set the response success to false
    $data["success"] = FALSE;
}


// Echo the encoded response
echo json_encode($data);
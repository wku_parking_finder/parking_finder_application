<?php
/**
 * Created by PhpStorm.
 * User: drew
 * Date: 3/27/17
 * Time: 9:25 AM
 */

//include_once("../php/parking_lot/parking_lot.class.php");
include_once("../php/sqler/sqler.class.php");
include_once("../php/parking_lot/change_password.class.php");


// Filter inputs
$password = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING);


// Start the session
session_start();

// Create data array
$data = [];

// Check if we have a result
if ($_SESSION["user_type"] == 1 || $_SESSION["user_type"] == 0 ) {
    
    $sql = new SQLer();
    
    //hash the password
    $hashedPass = $sql->hashPass($password);
    
    $changePassword = new Change_Password($hashedPass);
    
    // Save it and store the success
    $data["success"] = $changePassword->save();
}
else {
    // Set the response success to false
    $data["success"] = false;
}

// Echo the encoded response
echo json_encode($data);

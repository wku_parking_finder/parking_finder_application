<?php
/**
 * Created by PhpStorm.
 * User: drew
 * Date: 4/3/17
 * Time: 10:18 AM
 */

// Filter the sorting type input
$newSorting = filter_input(INPUT_POST, 'newSorting', FILTER_SANITIZE_STRING);

// Start the session
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

// Set the sorting preference
$_SESSION["sorting"] = $newSorting;
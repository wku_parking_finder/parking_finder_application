<?php
/**
 * Created by PhpStorm.
 * User: drew
 * Date: 4/30/17
 * Time: 4:26 PM
 */

include_once("../php/view/detailed_parking_lot_view.class.php");
include_once("../php/parking_lot/parking_lot.class.php");
include_once("../php/parking_lot/parking_lot_floor.class.php");

// Filter inputs
$lotId = filter_input(INPUT_POST, 'lotId', FILTER_SANITIZE_NUMBER_INT);

// Create the data array
$data = ["success" => TRUE, "latestLotView" => Detailed_Parking_Lot_View::getDetailedLotView($lotId)];

// Echo the json encoded array
echo json_encode($data);
<?php
/**
 * Created by PhpStorm.
 * User: drew
 * Date: 3/27/17
 * Time: 9:25 AM
 */

include_once("../php/parking_lot/parking_lot.class.php");
include_once("../php/sqler/sqler.class.php");
include_once("../php/view/control_panel_view.class.php");

// Filter inputs
$lotName = filter_input(INPUT_POST, 'lotName', FILTER_SANITIZE_STRING);
$lotDescription = filter_input(INPUT_POST, 'lotDescription', FILTER_SANITIZE_STRING);
$lotPhoto = filter_input(INPUT_POST, 'lotPhoto', FILTER_SANITIZE_STRING);

// Start the session
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

// Create data array
$data = [];

// Check if we have a result
if ($_SESSION["user_type"] == 1) {
    // Create the new parking lot
    $parkingLot = new Parking_Lot($lotName, $lotDescription, $lotPhoto);
    // Save it and store the success
    $data["success"] = $parkingLot->save();
    // Add the new lot table to be updated
    $data["newLotsTable"] = Control_Panel_View::getParkingLotsTable();
}
else {
    // Set the response success to false
    $data["success"] = false;
}


// Echo the encoded response
echo json_encode($data);

/**
 * Created by ant45527 on 2/13/2017.
 */

// Determine the server root
var root = window.location.href;
// Track refresh time for the main lot table
var lotRefreshTimer = null;
// Track refresh time for detailed lot view
var detailedLotRefreshTimer = null;

// Set timers on ready
$(document).ready(function() {
    // Fade in list if not visible
    if (!$("#parking_lot_list").is(":visible")) {
        $("#parking_lot_list").fadeIn(300);
    }
    // Start the main refresh timer
    lotRefreshTimer = setInterval(function() {
        // Don't make AJAX call if no parking lot list is present
        if (!$("#parking_lot_list").is(":visible") || $(".detailed_parking_lot").is(":visible")) {
            return;
        }
        // Request the latest lot table
        $.ajax({
            url: 'ajax/get_latest_lot_table.ajax.php',
            type: 'POST',
            data: {
                AJAX: true
            },
            success: function(data) {
                // Don't show response if interrupted by user click
                if (!$("#parking_lot_list").is(":visible") || $(".detailed_parking_lot").is(":visible")) {
                    return;
                }

                // Parse the response
                var jData = JSON.parse(data);

                // Check if successful
                if (jData["success"] === true) {
                    console.log("Updated the lots view!");
                    // Get scroll
                    var scrolltop = $("#parking_lot_list").scrollTop();
                    // Hide the list
                    $("#parking_lot_list").hide();
                    // Update the icon
                    $("#parking_lot_list").replaceWith(jData["latestLotsTable"]);
                    // Show the list
                    $("#parking_lot_list").fadeIn(300);
                    // Update the scroll
                    $("#parking_lot_list").scrollTop(scrolltop);
                }
            },
            error: function() {
                console.log("Error with updating the main lot view!");
            }
        });
    }, 8000);
    // Start the detailed refresh timertimer
    detailedLotRefreshTimer = setInterval(function() {
        // Don't make AJAX call if no detailed parking lot view is present
        if ($("#parking_lot_list").is(":visible") || !$(".detailed_parking_lot").is(":visible")) {
            return;
        }
        // Determine the lot id
        var lotId = $(".detailed_parking_lot").data("lot_id");
        // Request the latest lot table
        $.ajax({
            url: 'ajax/get_latest_lot_view.ajax.php',
            type: 'POST',
            data: {
                AJAX: true,
                lotId: lotId
            },
            success: function(data) {
                // Don't show response if interrupted by user click
                if ($("#parking_lot_list").is(":visible") || !$(".detailed_parking_lot").is(":visible")) {
                    return;
                }

                // Parse the response
                var jData = JSON.parse(data);

                // Check if successful
                if (jData["success"] === true) {
                    console.log("Updated the detailed lot view!");
                    // Update the icon
                    $(".detailed_parking_lot").replaceWith(jData["latestLotView"]);
                    // Make visibile
                    $(".detailed_parking_lot").fadeIn(300);
                }
            },
            error: function() {
                console.log("Error with updating the detailed lot view!");
            }
        });
    }, 8000);
});

// Toggle the floor to be active or inactive
function toggleFloorStatus(img) {
    // Get the floor id
    var floorId = $(img).parents("tr").first().data("floor_id");
    // Get the status
    var status = $(img).data("status");

    // Request status toggle
    $.ajax({
        url: 'ajax/update_floor_status.ajax.php',
        type: 'POST',
        data: {
            AJAX: true,
            floorId: floorId,
            status: (status ? 1 : 0)
        },
        success: function(data) {
            // Parse the response
            var jData = JSON.parse(data);

            // Check if successful
            if (jData["success"] === true) {
                // Update the icon
                $(img).replaceWith(jData["newFloorStatus"]);

                // Notify of success
                var message = "";
                if (!status) {
                    message = "Floor successfully set to active!";
                }
                else {
                    message = "Floor successfully set to inactive!"
                }

                Lobibox.notify("success", {
                    msg: message,
                    size: "mini",
                    delay: 3000,
                    sound: false
                });
            }
        },
        error: function() {
            console.log("Error with setting floor status!");
        }
    });
}

// Respond to filtering checkbox click
function updateFiltering(checkbox) {
    // Get the checkbox inputs values
    var closed = $(checkbox).is(":checked");

    // Update filtering
    $.ajax({
        url: 'ajax/update_filtering.ajax.php',
        type: 'POST',
        data: {
            AJAX: true,
            closed: (closed ? 1 : 0)
        },
        success: function(data) {
            // Refresh the page
            window.location.reload();
        },
        error: function() {
            console.log("Error with updating filtering!");
        }
    });
}

// Toggle the lot to be active or inactive
function toggleLotStatus(img) {
    // Get the lot id
    var lotId = $(img).parents("tr").first().data("lot_id");
    // Get the status
    var status = $(img).data("status");

    // Request status toggle
    $.ajax({
        url: 'ajax/update_lot_status.ajax.php',
        type: 'POST',
        data: {
            AJAX: true,
            lotId: lotId,
            status: (status ? 1 : 0)
        },
        success: function(data) {
            // Parse the response
            var jData = JSON.parse(data);

            // Check if successful
            if (jData["success"] === true) {
                // Update the icon
                $(img).replaceWith(jData["newLotStatus"]);

                // Notify of success
                var message = "";
                if (!status) {
                    message = "Lot successfully set to active!";
                }
                else {
                    message = "Lot successfully set to inactive!"
                }

                Lobibox.notify("success", {
                    msg: message,
                    size: "mini",
                    delay: 3000,
                    sound: false
                });
            }
        },
        error: function() {
            console.log("Error with setting lot status!");
        }
    });
}

// Close the floor corresponding to the checkbox
function respondToFloorCloseCheck(checkbox) {
    // Get the checkbox value
    var close = $(checkbox).is(":checked");
    // Get the lot id from the parent row
    var floorId = $(checkbox).parents("tr").first().data("floor_id");

    // Request close
    $.ajax({
        url: 'ajax/update_floor_closed.ajax.php',
        type: 'POST',
        data: {
            AJAX: true,
            floorId: floorId,
            close: (close ? 1 : 0)
        },
        success: function(data) {
            // Parse the response
            var jData = JSON.parse(data);

            // Check if successful
            if (jData["success"] === true) {
                var message = "";
                if (close) {
                    message = "Floor successfully closed!";
                }
                else {
                    message = "Floor successfully opened!"
                }

                Lobibox.notify("success", {
                    msg: message,
                    size: "mini",
                    delay: 3000,
                    sound: false
                });
            }
        },
        error: function() {
            console.log("Error with closing floor!");
        }
    });
}

// Close the lot corresponding to the checkbox
function respondToLotCloseCheck(checkbox) {
    // Get the checkbox value
    var close = $(checkbox).is(":checked");
    // Get the lot id from the parent row
    var lotId = $(checkbox).parents("tr").first().data("lot_id");

    // Request close
    $.ajax({
        url: 'ajax/update_lot_closed.ajax.php',
        type: 'POST',
        data: {
            AJAX: true,
            lotId: lotId,
            close: (close ? 1 : 0)
        },
        success: function(data) {
            // Parse the response
            var jData = JSON.parse(data);

            // Check if successful
            if (jData["success"] === true) {
                var message = "";
                if (close) {
                    message = "Lot successfully closed!";
                }
                else {
                    message = "Lot successfully opened!"
                }

                Lobibox.notify("success", {
                    msg: message,
                    size: "mini",
                    delay: 3000,
                    sound: false
                });
            }
        },
        error: function() {
            console.log("Error with closing lot!");
        }
    });
}

// Changes the lot sorting
function sortLotsBy(select) {
    // Get the value of the select
    var newSorting = $(select).val();

    // Request new sorting
    $.ajax({
        url: 'ajax/change_lot_sorting.ajax.php',
        type: 'POST',
        data: {
            AJAX: true,
            newSorting: newSorting
        },
        success: function(data) {
            // Refresh the page
            window.location.reload();
        },
        error: function() {
            console.log("Error with creating new parking lot!");
        }
    });
}

// Shows the floors in the control panel
function viewFloors(element) {
    // Determine the parent row
    var row = $(element).parents("tr").first();

    // Expand the floor table for the lot (in the next tr)
    $(row).next("tr").slideToggle(600);
}

// Uploads the lot photo
function uploadLotPhoto() {
    var file_data = $("#create_parking_lot_container input[name='lot_photo']").prop("files")[0];

    // Do nothing if no file exists
    if (file_data === undefined) {
        // Inform of no file selected
        Lobibox.notify("error", {
            msg: "Please select a file prior to attempting an upload!",
            size: "mini",
            delay: 3000,
            sound: false
        });
        return;
    }

    var form_data = new FormData();
    form_data.append("file", file_data);
    $.ajax({
        url: 'ajax/upload_lot_photo.ajax.php',
        dataType: 'text',
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        type: 'POST',
        success: function(data){
            var jData = JSON.parse(data);
            if (jData["success"] == false) {
                console.log(jData["message"]);
            }
            else {
                // Inform user of successful upload
                Lobibox.notify("success", {
                    msg: "Successfully uploaded the lot photo!",
                    size: "mini",
                    delay: 3000,
                    sound: false
                });

                // Replace the no media image
                $("img.lot_photo_upload").attr("src", root + 'uploads/' + jData["new_image"]);

                // Change the input value
                $("img.lot_photo_upload").attr("data-image_name", jData["new_image"]);
            }
        }
    });
}

// Responds to the new floor form submission
function createNewFloor(button) {
    // Determine the lot id
    var formRow = $(button).parents("tr").first().prev("tr");
    var lotId = formRow.attr("data-lot_id");

    // Gather the form input values
    var floorPermitRequirement = formRow.find("select").val();
    var floorCapacity = formRow.find("td:nth-child(2) input").val();
    var floorHandicapCapacity = formRow.find("td:nth-child(3) input").val();
    var floorClosed = formRow.find("td:nth-child(4) input").is(":checked");
    var floorStatus = formRow.find("td:nth-child(5) img").attr("src") == "images/active.png";

    // Check that the floor capacity is not empty
    if (floorCapacity.length == 0) {
        // Set the input border to red
        formRow.find("td:nth-child(2)").css("border", "1px solid red");
        // Set the error message
        formRow.next("tr").find(".error_message").text("The floor capacity cannot be empty!");
        // Don't process form
        return;
    }

    // Reset floor capacity input css
    formRow.find("td:nth-child(2)").css("border", "1px solid #333333");

    // Check that the floor handicap capacity is not empty
    if (floorHandicapCapacity.length == 0) {
        // Set the input border to red
        formRow.find("td:nth-child(3)").css("border", "1px solid red");
        // Set the error message
        formRow.next("tr").find(".error_message").text("The floor handicap capacity cannot be empty!");
        // Don't process form
        return;
    }

    // Reset floor handicap capacity description css
    formRow.find("td:nth-child(3)").css("border", "1px solid #333333");

    // Request access to the new parking lot popup
    $.ajax({
        url: 'ajax/create_new_floor.ajax.php',
        type: 'POST',
        data: {
            AJAX: true,
            lotId : lotId,
            floorPermitRequirement : floorPermitRequirement,
            floorCapacity : floorCapacity,
            floorHandicapCapacity :  floorHandicapCapacity,
            floorClosed : floorClosed,
            floorStatus : floorStatus
        },
        success: function(data) {
            // Parse the response
            var jData = JSON.parse(data);

            // Check for adequate privileges
            if (jData["success"] == true) {
                // Inform the user of successful lot creation
                Lobibox.notify("success", {
                    msg: "Successfully created new parking lot floor!",
                    size: "mini",
                    delay: 3000,
                    sound: false
                });
                // Append the new floor to the floors table for this lot
                $(formRow).prev("tr").after(jData["newFloorRow"]);
            }
            else {
                // Inform of inadequate privileges
                Lobibox.notify("error", {
                    msg: "You do not have the proper permissions to perform this action!",
                    size: "mini",
                    delay: 3000,
                    sound: false
                });
            }
        },
        error: function() {
            console.log("Error with creating new parking lot!");
        }
    });
}

function createNewPassword() {
    // Gather the form input values
        var password = $("#create_change_password_container input[name='password']").val();
    
    // Check that the new password is not empty
    if (password.length == 0) {
        // Set the input border to red
        $("#create_change_password_container input[name='password']").css("border", "1px solid red");
        // Set the error message
        $("#create_change_password_container .error_message").text("The new password cannot be empty!");
        // Don't process form
        return;
    }
    
    // Reset password input css
    $("#create_change_password_container input[name='password']").css("border", "1px solid #333333");

    $.ajax({
        url: 'ajax/create_change_password.ajax.php',
        type: 'POST',
        data: {
            AJAX: true,
            password: password
        },
        success: function(data) {
            // Parse the response
            var jData = JSON.parse(data);

            // Check for adequate privileges
            if (jData["success"] == true) {
                // Inform the user of successful password change
                Lobibox.notify("success", {
                    msg: "Successfully changed the password!",
                    size: "mini",
                    delay: 3000,
                    sound: false
                });
                // Close this popup
                closeChangePasswordPopup();
            }
            else {
                // Inform of inadequate privileges
                Lobibox.notify("error", {
                    msg: "You do not have the proper permissions to perform this action!",
                    size: "mini",
                    delay: 3000,
                    sound: false
                });
            }
        },
        error: function() {
            console.log("Error with creating new password!");
        }
    });
}

// Responds to the new parking lot form submission
function createNewParkingLot() {
    // Gather the form input values
    var lotName = $("#create_parking_lot_container input[name='lot_name']").val();
    var lotDescription = $("#create_parking_lot_container textarea[name='lot_description']").val();
    var lotPhoto = $("#create_parking_lot_container input[name='lot_photo']").val();

    // Check that the lot name is not empty
    if (lotName.length == 0) {
        // Set the input border to red
        $("#create_parking_lot_container input[name='lot_name']").css("border", "1px solid red");
        // Set the error message
        $("#create_parking_lot_container .error_message").text("The lot name cannot be empty!");
        // Don't process form
        return;
    }

    // Reset lot name input css
    $("#create_parking_lot_container input[name='lot_name']").css("border", "1px solid #333333");

    // Check that the lot description is not empty
    if (lotDescription.length == 0) {
        // Set the input border to red
        $("#create_parking_lot_container textarea[name='lot_description']").css("border", "1px solid red");
        // Set the error message
        $("#create_parking_lot_container .error_message").text("The lot description cannot be empty!");
        // Don't process form
        return;
    }

    // Reset lot name description css
    $("#create_parking_lot_container textarea[name='lot_description']").css("border", "1px solid #333333");

    // Check that the lot photo is not empty
    if (lotPhoto.length == 0) {
        // Set the input border to red
        $("#create_parking_lot_container input[name='lot_photo']").css("border", "1px solid red");
        // Set the error message
        $("#create_parking_lot_container .error_message").text("The lot photo cannot be empty!");
        // Don't process form
        return;
    }

    // Reset lot photo input css
    $("#create_parking_lot_container input[name='lot_photo']").css("border", "1px solid #333333");


    // Request access to the new parking lot popup
    $.ajax({
        url: 'ajax/create_new_parking_lot.ajax.php',
        type: 'POST',
        data: {
            AJAX: true,
            lotName : lotName,
            lotDescription : lotDescription,
            lotPhoto :  $("img.lot_photo_upload").attr("data-image_name")
        },
        success: function(data) {
            // Parse the response
            var jData = JSON.parse(data);

            // Check for adequate privileges
            if (jData["success"] == true) {
                // Inform the user of successful lot creation
                Lobibox.notify("success", {
                    msg: "Successfully created new parking lot: '" + lotName + "'!",
                    size: "mini",
                    delay: 3000,
                    sound: false
                });
                // Close this popup
                closeCreateParkingLotPopup();
                // Replace the old lots table
                $(".lot_table").replaceWith(jData["newLotsTable"]);

            }
            else {
                // Inform of inadequate privileges
                Lobibox.notify("error", {
                    msg: "You do not have the proper permissions to perform this action!",
                    size: "mini",
                    delay: 3000,
                    sound: false
                });
            }
        },
        error: function() {
            console.log("Error with creating new parking lot!");
        }
    });
}

// Closes the create parking lot popup
function closeCreateParkingLotPopup() {
    $("#create_parking_lot_container").fadeOut(300, function() {
        $(this).remove();
    });
    $("#create_parking_lot_clickout").fadeOut(300, function() {
        $(this).remove();
    });
}

function closeChangePasswordPopup() {
    $("#create_change_password_container").fadeOut(300, function() {
        $(this).remove();
    });
    $("#create_change_password_clickout").fadeOut(300, function() {
        $(this).remove();
    });
}


// Opens the popup for changing password
function openChangePasswordPopup() {
    // Request access to the new parking lot popup
    $.ajax({
        url: 'ajax/open_change_password_popup.ajax.php',
        type: 'POST',
        data: {
            AJAX: true
        },
        success: function(data) {
            // Parse the response
            var jData = JSON.parse(data);

            // Check for successful login
            if (jData["success"] == true) {
                // Append the view
                $(jData["toAppend"]).appendTo("body").fadeIn(300);
            }
            else {
                // Inform of inadequate privileges
                Lobibox.notify("error", {
                    msg: "You do not have the proper permissions to perform this action!",
                    size: "mini",
                    delay: 3000,
                    sound: false
                });
            }
        },
        error: function() {
            console.log("Error with changing the password popup!");
        }
    });
}

// Opens the popup for creating a new parking lot
function openNewParkingLotPopup() {
    // Request access to the new parking lot popup
    $.ajax({
        url: 'ajax/open_new_parking_lot_popup.ajax.php',
        type: 'POST',
        data: {
            AJAX: true
        },
        success: function(data) {
            // Parse the response
            var jData = JSON.parse(data);

            // Check for successful login
            if (jData["success"] == true) {
                // Append the view
                $(jData["toAppend"]).appendTo("body").fadeIn(300);
            }
            else {
                // Inform of inadequate privileges
                Lobibox.notify("error", {
                    msg: "You do not have the proper permissions to perform this action!",
                    size: "mini",
                    delay: 3000,
                    sound: false
                });
            }
        },
        error: function() {
            console.log("Error with opening new parking lot popup!");
        }
    });
}

// Submits the login credentials and attempts to log the user in
function login() {
    // Determine the provided credentials
    var username = $("#login_form input[name='username']");
    var password = $("#login_form input[name='password']");

    // Request access to the control panel view
    $.ajax({
        url: 'ajax/login_to_control_panel.ajax.php',
        type: 'POST',
        data: {
            AJAX: true,
            username: username.val(),
            password: password.val()
        },
        success: function(data) {
            // Parse the response
            var jData = JSON.parse(data);

            // Check for successful login
            if (jData["success"] == true) {
                // Clear the password box
                $(password).val("");

                // Close the login form
                toggleLoginForm();

                // Append the view
                $(jData["toAppend"]).appendTo("body");

                // Hide the floors tables
                $("tr.floors_row").hide();

                // Fade in
                $("#control_panel_container").fadeIn(300);
                $("#control_panel_clickout").fadeIn(300);
            }
            else {
                // Indicate bad login credentials
                $(username).css("border", "1px solid red");
                $(password).css("border", "1px solid red");
                $("#login_form .error_message").text("Invalid credentials.");
            }

        },
        error: function() {
            console.log("Error with logging into the control panel!");
        }
    });
}


// Closes the control panel
function closeControlPanel() {
    // Request the corresponding detailed view
    $.ajax({
        url: 'ajax/logout.ajax.php',
        type: 'POST',
        data: {
            AJAX: true
        },
        success: function() {
            // Close the control panel
            $("#control_panel_container").fadeOut(300, function() {
                $(this).remove();
            });
            $("#control_panel_clickout").fadeOut(300, function() {
                $(this).remove();
            });
        },
        error: function() {
            console.log("Error with closing the control panel!");
        }
    });
}


// Shows/hides the login form
function toggleLoginForm() {
    // Show the form
    $("#login_form").slideToggle(300, function() {
        // Check if showing the form
        if ($("#login_form").is(":visible")) {
            // Set the focus to the user input
            $("#login_form input[name='username']").focus();
        }
    });
}

// Return to the main list
function returnToParkingLotList() {
    // Fade out the detailed view and remove it
    $(".detailed_parking_lot").fadeOut(300, function() {
        $(this).remove();
    });
    // Fade out the return button and remove it
    $("#return_button").fadeOut(300, function() {
        $(this).remove();

        // Fade in the parking lot list
        $("#parking_lot_list").fadeIn(300);
    });
}

// Shows the detailed view for the selected lot
function showDetailedLotView(div) {
    // Determine the lot id
    var lotId = $(div).data("lot_id");

    // Request the corresponding detailed view
    $.ajax({
        url: 'ajax/get_detailed_lot_view.ajax.php',
        type: 'POST',
        data: {
            AJAX: true,
            lotId: (lotId)
        },
        success: function(data) {
            // Parse the response
            var jData = JSON.parse(data);

            // Append the detailed view
            $("body").append(jData["detailed_view"]);

            // Fade out the parking lot list
            $("#parking_lot_list").fadeOut(300, function() {
                // Fade in the view
                $(".detailed_parking_lot").fadeIn(300);
                // Fade in the return button
                $("#return_button").fadeIn(300);
            });

        },
        error: function() {
            console.log("Error with showing detailed lot view!");
        }
    });
}
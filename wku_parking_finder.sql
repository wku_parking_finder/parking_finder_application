-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 30, 2017 at 07:38 PM
-- Server version: 5.7.18-0ubuntu0.16.04.1
-- PHP Version: 7.0.15-0ubuntu0.16.04.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wku_parking_finder`
--
CREATE DATABASE IF NOT EXISTS `wku_parking_finder` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `wku_parking_finder`;

-- --------------------------------------------------------

--
-- Table structure for table `parking_lots`
--

CREATE TABLE `parking_lots` (
  `lot_id` int(8) NOT NULL,
  `lot_name` varchar(30) NOT NULL,
  `lot_desc` varchar(255) NOT NULL,
  `lot_pic` varchar(40) NOT NULL,
  `closed` tinyint(4) NOT NULL DEFAULT '0',
  `display_status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `parking_lots`
--

INSERT INTO `parking_lots` (`lot_id`, `lot_name`, `lot_desc`, `lot_pic`, `closed`, `display_status`) VALUES
(1, 'Adams Street Lot', 'Located between Kentucky St and Adams St, Adams Lot provides parking for both housing and faculty.', 'adams-lot.jpg', 1, 1),
(2, 'Alumni Square Garage', 'The Alumni Square Garage is located on Alumni Avenue at Kentucky Street.  The Topper Transit RED LINE picks up passengers on Center Street next to the Augenstein Alumni Center on its way to campus from 7:30 AM - 8:30 PM weekdays.', 'asg.png', 0, 1),
(4, 'Barnes Lot', 'Located next to the Pearce Ford Lot, Barnes Lot provides parking for housing students.', '', 0, 0),
(5, 'Cherry Lot', 'Cherry Lot is located next to Cherry Hall, and provides parking for faculty members.', 'cherry-lot.jpg', 0, 1),
(6, 'Chestnut Street North Lot', 'Located on Chestnut St next to Regents Ave, Chestnut Street North Lot provides parking for faculty members.', 'chestnut-north.jpg', 0, 1),
(7, 'Chestnut Street South Lot', 'Located on Chestnut St, between East 13th and 14th St, Chestnut Street South Lot provides parking for commuters and faculty.', 'chestnut-south.jpg', 0, 1),
(8, 'College Hill Lot', 'Located on College Heights Blvd., College Hill Lot provides parking for faculty.', 'college-hill.jpg', 0, 1),
(9, 'Creason Lot', 'Located on Creason St. Creason Lot provides housing parking.', 'creason-lot.jpg', 0, 1),
(10, 'Denes Field Lot', 'Located on University Blvd, Denes Field lot provides general parking for both students and faculty.', 'denes-field.jpg', 0, 1),
(11, 'Diddle North Lot', 'Located next to the Diddle Arena, the Diddle North Lot provides parking for faculty and staff.', 'diddle-north.jpg', 0, 1),
(12, 'Diddle South Lot', 'Located next to the Diddle Arena, the Diddle South Lot provides paid public parking.', 'diddle-south.jpg', 0, 1),
(13, 'Diddle West Lot', 'Located front of PS2, Diddle West Lot provides parking for faculty and staff members.', 'diddle-west.jpg', 0, 0),
(14, 'Gated Hilltop Lot', 'Gated Hilltop Lot is located next to the Industrial Engineering building.', 'gated-hilltop.jpg', 0, 0),
(15, 'Health Services Lot', 'Health Services Lot is located between Regents Ave. and Mass Media Technology Hall.', 'health-svc.jpg', 0, 0),
(16, 'Helm Lot', 'Located between the Helm Library and Industrial Engineering building, Helm lot provides parking for faculty and staff.', 'helm-lot.jpg', 0, 0),
(17, 'Jones Jaggers Lot', 'Located on University Blvd, Jones Jaggers Lot provides parking for faculty and Staff members.', 'jones-jaggers.jpg', 0, 0),
(18, 'Kentucky Street Lot', 'Located on Kentucky Street, the Kentucky Street Lot provides parking for both faculty and commuters.', 'ky-st.jpg', 0, 0),
(19, 'Lower Hub Lot', 'Located off of College Heights Blvd, the Lower Hub Lot provides parking for faculty and staff members.', 'lower-hub-lot.jpg', 0, 0),
(20, 'Mimosa Lot', 'Located next to the Adams-Whitaker Student Publications Center, Mimosa Lot provides parking for faculty and staff members.', 'mimosa-lot.jpg', 0, 0),
(21, 'Mimosa Point Lot', 'Located on Normal Street, next to the Normal Street North lot, Mimosa Point Lot provides parking for faculty and staff members.', '', 0, 0),
(22, 'Minton Lot', 'Located in front of Minton Hall, the Minton Lot provides housing parking.', 'minton-lot.jpg', 0, 0),
(23, 'Normal Lot', 'Located on Normal Street, the Normal lot provides "normal parking".', '', 0, 0),
(24, 'Ogden Lot', 'Located in front of College High Hall, he Ogden Lot provides parking for faculty and staff members.', 'ogden-lot.jpg', 0, 0),
(25, 'Parking Structure One', 'Located on College Heights Blvd, near Diddle Arena, PS1 provides both housing and faculty parking.', 'ps1.png', 0, 1),
(26, 'Parking Structure Two', 'Located between LT Smith Stadium and Diddle Arena, PS2 provides parking for commuters.', 'ps2.jpg', 0, 1),
(27, 'Pearce Ford Lot', 'Located next to the Pearce Ford Tower, the Pearce Ford Lot provides housing parking.', 'pft-lot.jpg', 0, 0),
(28, 'Poland Lot', 'Located between Keen Hall and Preston Health & Activities Center, the Poland Lot provides housing parking.', 'poland-lot.jpg', 0, 0),
(29, 'Regents Lot', 'Located next to Barnes Lot, the Regents Lot provides gated parking.', 'regents-lot.jpg', 0, 0),
(30, 'Russellville Road East Lot', 'Located behind the Service Supply Building, the Russellville Road East Lot provides general parking.', 'russellville-lot.jpg', 0, 0),
(31, 'Russellville Road West Lot', 'Located on Russellville Road, the Russellville Road West Lot provides parking for commuters.', '', 0, 0),
(32, 'Service Supply Lot', 'Located next to the Service Supply Building, the Service Supply Lot provides general parking.', 'svc-supply-lot.jpg', 0, 0),
(33, 'South Lawn Lot', 'Located on Avenue of Champions, the South Lawn Lot provides parking for faculty and staff members.', '', 0, 0),
(34, 'University Blvd Lot', 'Located on University Blvd, next to Normal Drive, the University Blvd lot provides parking for commuters.', 'unv-blvd-lot.jpg', 0, 0),
(35, 'Upper Hub Lot', 'Located on College Heights Blvd, the Upper Hub Lot provides parking for faculty and staff members.', 'hub-lot.jpg', 0, 0),
(36, 'West Stadium Lot', 'Located between the LT Smith Stadium and Denes Field, the West Stadium Lot provides parking for faculty and staff members.', 'west-stdm-lot.jpg', 0, 0),
(37, 'Test Lot', 'This is a test lot.', 'asg.png', 0, 1),
(38, 'Test Lot2', 'This is another test lot.', 'testlot.jpg', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `parking_lot_floors`
--

CREATE TABLE `parking_lot_floors` (
  `lot_id` int(11) NOT NULL,
  `floor_id` int(11) NOT NULL,
  `permit_id` int(11) NOT NULL,
  `closed` tinyint(1) NOT NULL COMMENT 'Floor closed',
  `closing_message` varchar(256) NOT NULL,
  `total_spots` int(255) NOT NULL,
  `total_handicap_spots` int(255) NOT NULL,
  `available_spots` int(255) NOT NULL,
  `available_handicap_spots` int(255) NOT NULL,
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `display_status` tinyint(1) NOT NULL COMMENT '1 to enable floor, 0 to disable/delete floor'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `parking_lot_floors`
--

INSERT INTO `parking_lot_floors` (`lot_id`, `floor_id`, `permit_id`, `closed`, `closing_message`, `total_spots`, `total_handicap_spots`, `available_spots`, `available_handicap_spots`, `last_update`, `display_status`) VALUES
(1, 1, 6, 0, '', 25, 5, 25, 5, '2017-04-03 21:53:29', 1),
(1, 2, 8, 0, '', 65, 5, 65, 5, '2017-04-03 21:55:04', 1),
(2, 3, 1, 0, '', 150, 10, 150, 10, '2017-04-24 14:31:46', 1),
(2, 4, 1, 0, '', 150, 10, 150, 10, '2017-04-24 14:32:02', 1),
(2, 5, 1, 0, '', 150, 150, 5, 5, '2017-04-24 14:32:14', 1),
(2, 6, 1, 0, '', 150, 5, 150, 5, '2017-04-24 14:32:26', 1),
(2, 7, 1, 0, '', 150, 0, 150, 0, '2017-04-24 14:32:38', 1),
(4, 8, 8, 0, '', 98, 2, 98, 2, '2017-04-10 00:14:28', 1),
(5, 9, 4, 0, '', 10, 2, 10, 2, '2017-04-10 00:19:22', 1),
(6, 10, 6, 0, '', 50, 0, 50, 0, '2017-04-10 00:18:45', 1),
(6, 11, 10, 0, '', 75, 2, 75, 2, '2017-04-24 14:14:41', 1),
(7, 12, 6, 0, '', 50, 50, 50, 50, '2017-04-11 20:48:26', 1),
(8, 13, 6, 0, '', 55, 5, 55, 5, '2017-04-11 20:53:47', 1),
(9, 14, 8, 0, '', 295, 5, 295, 5, '2017-04-11 20:54:27', 1),
(10, 15, 11, 0, '', 18, 2, 18, 2, '2017-04-11 20:57:33', 1),
(11, 16, 5, 0, '', 75, 5, 75, 5, '2017-04-11 20:58:16', 1),
(12, 17, 12, 0, '', 30, 5, 30, 5, '2017-04-11 20:59:49', 1),
(13, 18, 5, 0, '', 25, 5, 25, 5, '2017-04-11 21:00:22', 1),
(14, 19, 7, 0, '', 50, 0, 50, 0, '2017-04-24 14:16:04', 1),
(15, 20, 12, 0, '', 20, 5, 20, 5, '2017-04-11 21:05:09', 1),
(16, 21, 4, 0, '', 18, 2, 18, 2, '2017-04-11 21:05:33', 1),
(17, 22, 6, 0, '', 38, 2, 38, 2, '2017-04-11 21:07:25', 1),
(18, 23, 8, 0, '', 85, 5, 85, 5, '2017-04-11 21:07:58', 1),
(19, 24, 6, 0, '', 40, 0, 40, 0, '2017-04-11 21:08:36', 1),
(20, 25, 4, 0, '', 45, 5, 45, 5, '2017-04-11 21:09:04', 1),
(21, 26, 4, 0, '', 18, 2, 18, 2, '2017-04-11 21:09:47', 1),
(22, 27, 8, 0, '', 50, 5, 50, 5, '2017-04-11 21:10:37', 1),
(23, 28, 6, 0, '', 50, 5, 50, 5, '2017-04-11 21:11:52', 1),
(24, 29, 6, 0, '', 10, 0, 10, 0, '2017-04-11 21:12:22', 1),
(25, 30, 6, 0, '', 95, 5, 95, 5, '2017-04-11 21:13:18', 1),
(25, 31, 6, 0, '', 100, 0, 100, 0, '2017-04-11 21:13:27', 1),
(25, 32, 6, 0, '', 100, 0, 100, 0, '2017-04-11 21:13:32', 1),
(25, 33, 8, 0, '', 100, 0, 100, 0, '2017-04-11 21:13:43', 1),
(25, 34, 8, 0, '', 100, 0, 100, 0, '2017-04-11 21:13:50', 1),
(25, 35, 8, 0, '', 100, 0, 100, 0, '2017-04-11 21:13:57', 1),
(25, 36, 8, 0, '', 100, 0, 100, 0, '2017-04-11 21:14:01', 1),
(26, 37, 10, 0, '', 195, 5, 195, 5, '2017-04-11 21:15:09', 1),
(26, 38, 10, 0, '', 195, 5, 195, 5, '2017-04-11 21:15:15', 1),
(26, 39, 10, 0, '', 200, 0, 200, 0, '2017-04-11 21:15:20', 1),
(26, 40, 10, 0, '', 200, 0, 200, 0, '2017-04-11 21:15:24', 1),
(26, 41, 10, 0, '', 200, 0, 200, 0, '2017-04-11 21:15:25', 1),
(27, 42, 8, 0, '', 145, 5, 145, 5, '2017-04-11 21:16:16', 1),
(28, 43, 8, 0, '', 195, 5, 195, 5, '2017-04-12 14:40:17', 1),
(29, 44, 1, 0, '', 45, 5, 45, 5, '2017-04-12 14:41:17', 1),
(30, 45, 1, 0, '', 75, 5, 75, 5, '2017-04-12 14:43:34', 1),
(31, 46, 9, 0, '', 145, 5, 145, 5, '2017-04-12 14:44:00', 1),
(32, 47, 1, 0, '', 45, 5, 45, 5, '2017-04-12 14:44:39', 1),
(33, 48, 5, 0, '', 30, 5, 30, 5, '2017-04-12 14:48:38', 1),
(34, 49, 9, 0, '', 95, 5, 95, 5, '2017-04-12 14:49:43', 1),
(35, 50, 4, 0, '', 28, 2, 28, 2, '2017-04-17 14:11:53', 1),
(36, 51, 5, 0, '', 18, 2, 18, 2, '2017-04-17 14:12:33', 1);

-- --------------------------------------------------------

--
-- Table structure for table `parking_permits`
--

CREATE TABLE `parking_permits` (
  `permit_id` int(64) NOT NULL,
  `permit_name` varchar(128) NOT NULL,
  `permit_status` tinyint(1) NOT NULL COMMENT '1 to enable permit type, 0 to disable permit type'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `parking_permits`
--

INSERT INTO `parking_permits` (`permit_id`, `permit_name`, `permit_status`) VALUES
(1, 'Alumni Sq Garage', 1),
(2, 'Gated Hilltop', 1),
(3, 'Gated Regents', 1),
(4, 'FS1', 1),
(5, 'FS2', 1),
(6, 'FS3', 1),
(7, 'GH', 1),
(8, 'H', 1),
(9, 'C1', 1),
(10, 'C2', 1),
(11, 'AP', 1),
(12, 'Public', 1),
(13, 'W', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` varchar(64) NOT NULL,
  `password` varchar(128) NOT NULL,
  `user_type` tinyint(1) NOT NULL COMMENT 'denotes the user type, super-admin or mod',
  `user_status` tinyint(1) NOT NULL COMMENT '1 means enabled, 0 means disabled'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Admin & mod user account info';

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `password`, `user_type`, `user_status`) VALUES
('pntadmin', 'be387f089d0da0f8836504ec0193b227270293dd', 1, 1),
('pntmod', '6b03c03e67e9ccae3371012d42d2c4932071110a', 0, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `parking_lots`
--
ALTER TABLE `parking_lots`
  ADD PRIMARY KEY (`lot_id`);

--
-- Indexes for table `parking_lot_floors`
--
ALTER TABLE `parking_lot_floors`
  ADD PRIMARY KEY (`floor_id`);

--
-- Indexes for table `parking_permits`
--
ALTER TABLE `parking_permits`
  ADD PRIMARY KEY (`permit_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `parking_lots`
--
ALTER TABLE `parking_lots`
  MODIFY `lot_id` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT for table `parking_lot_floors`
--
ALTER TABLE `parking_lot_floors`
  MODIFY `floor_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;
--
-- AUTO_INCREMENT for table `parking_permits`
--
ALTER TABLE `parking_permits`
  MODIFY `permit_id` int(64) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

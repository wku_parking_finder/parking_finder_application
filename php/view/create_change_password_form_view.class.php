<?php

/**
 * Created by PhpStorm.
 * User: drew
 * Date: 3/21/17
 * Time: 9:53 AM
 */
class Create_Change_Password_Form_View
{
    public function __toString() {
        // Return the container
        return $this->getClickout() . $this->getContainer();
    }

    protected function getClickout() {
        // Return the clickout
        return "<div id='create_change_password_clickout' onclick='closeChangePasswordPopup();'></div>";
    }   

    protected function getContainer() {
        // Create the container
        $container = "<div id='create_change_password_container'>";

        // Create the container header
        $container .= "<h3>Please Enter Your New Password</h3><br><br>";

        // Create the name input and label
        $container .= "<label>Your New Password: </label>";
        $container .= "<input type='text' name='password' placeholder='New Password' maxlength='30'>";
        $container .= "<br><br>";

        // Add the form validation notifier
        $container .= "<span class='error_message'></span><br>";

        // Create the submit button
        $container .= "<button onclick='createNewPassword();' class='create_button'>Change Password</button>";

        // Close it and return it
        return $container . "</div>";
    }
}
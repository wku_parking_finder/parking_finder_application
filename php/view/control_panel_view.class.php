<?php

/**
 * Created by PhpStorm.
 * User: drew
 * Date: 2/26/17
 * Time: 9:04 PM
 */

include_once(dirname(__DIR__) . "/parking_lot/parking_lot.class.php");
include_once(dirname(__DIR__) . "/parking_lot/parking_lot_floor.class.php");

class Control_Panel_View
{
    protected $userType; // 0 for mod, 1 for admin

    public function __construct() {
        // Set this user type to reflect the requested view type
        $this->userType = $_SESSION["user_type"];
    }

    public function __toString() {
        // Return the container
        return $this->getClickout() . $this->getContainer();
    }

    protected function getClickout() {
        // Return the clickout
        return "<div id='control_panel_clickout' onclick='closeControlPanel();'></div>";
    }

    protected function getContainer() {
        // Create the container
        $container = "<div id='control_panel_container'>";

        // Add the header
        $container .= "<h3>WKU Parking Lots</h3><br>";

        // Check if admin
        if ($this->userType == 1) {
            // Add the new parking lot button
            $container .= "<button class='create_button' onclick='openNewParkingLotPopup();'>+ New Parking Lot</button>";
        }
            $container .= "<button class='edit_button' onclick='openChangePasswordPopup();'>Change Password</button><br><br>";

        // Get the parking lot table for the administrator
        $container .= $this->getParkingLotsTable() . "<br>";

        // Close it and return it
        return $container . "</div>";
    }

    public static function getParkingLotsTable() {
        // Create the table
        $table = "<table class='lot_table' id='lots_control_panel_table'>";

        // Create the header row
        $table .= "<tr>
                       <th>" . "Lot Photo" . "</th>
                       <th>" . "Lot Name" . "</th>
                       <th>" . "Lot Description" . "</th>
                       <th>" . "Lot Status" . "</th>
                       <th>" . "Closed" . "</th>
                       <th>" . "Lot Floors" . "</th>
                   </tr>";

        // Get all parking lots
        $lots = Parking_Lot::getAllLots();

        // Iterate through all
        foreach ($lots as $lot) {
            // Create the table row
            $table .= "<tr data-lot_id='" . $lot->id . "'>";

            // Add the photo cell
            $table .= "<td><img style='height: 60px;' src='" .
                (strlen($lot->pic) == 0 ? "images/no_media.png" : ("uploads/" . $lot->pic ) ) .
                "'/></td>";

            // Add the label header cell
            $table .= "<td>" . $lot->title . "</td>";

            // Add the description
            $table .= "<td>" . (strlen($lot->description) == 0 ? "No description provided" : $lot->description) . "</td>";

            // Get the status icon
            $table .= "<td>" . self::getLotStatusIcon($lot) . "</td>";

            // Check if is closed
            if ($lot->closed) {
                // Add the lot closed input
                $table .= "<td><input type='checkbox' checked onchange='respondToLotCloseCheck(this);'></td>";
            }
            else {
                // Add the lot closed input
                $table .= "<td><input type='checkbox' onchange='respondToLotCloseCheck(this);'></td>";
            }

            // Add the 'view floors' button cell
            $table .= "<td><button onclick='viewFloors(this);' class='edit_button'> &#9660; View Floors</button></td>";

            // End the row
            $table .= "</tr>";

            // Create a row for the floors
            $table .= "<tr class='floors_row'><td colspan='6' style='padding: 0;'>";

            // Add the floors table to this row
            $table .= self::getParkingLotFloorsTableForLotId($lot->id);

            // End this row
            $table .= "</td></tr>";
        }

        // Close the table
        $table .= "</table>";

        // Return it
        return $table;
    }

    public static function getFloorStatusIcon($floor) {
        // Create the icon
        $icon = "";
        // Check perms for status updating
        if ($_SESSION["user_type"] == 1) {
            // Add the status with an onclick
            $icon = "<img class='status' onclick='toggleFloorStatus(this);' 
                    src='images/" . ($floor->status == 1 ? "active.png" : "not_active.png")
                . "' data-status='$floor->status'/>";
        }
        else {
            // Add the status without an onclick
            $icon = "<img class='status' src='images/" . ($floor->status == 1 ? "active.png" : "not_active.png")
                . "' data-status='$floor->status'"
                . "/>";
        }
        return $icon;
    }

    public static function getLotStatusIcon($lot) {
        // Create the icon
        $icon = "";
        // Check perms for status updating
        if ($_SESSION["user_type"] == 1) {
            // Add the status with an onclick
            $icon = "<img class='status' onclick='toggleLotStatus(this);' 
                    src='images/" . ($lot->status == 1 ? "active.png" : "not_active.png")
                . "' data-status='$lot->status'/>";
        }
        else {
            // Add the status without an onclick
            $icon = "<img class='status' src='images/" . ($lot->status == 1 ? "active.png" : "not_active.png")
                . "' data-status='$lot->status'"
                . "/>";
        }
        return $icon;
    }

    private static function getParkingLotFloorsTableForLotId($lotId) {
        // Create the table
        $table = "<table class='lot_table' style='border: 4px solid #B01E24;' id='lot_floors_control_panel_table_" . $lotId . "'>";

        // Create the header row
        $table .= "<tr>
                       <th>" . "Floor Permit" . "</th>
                       <th>" . "Floor Capacity" . "</th>
                       <th>" . "Floor Handicap Capacity" . "</th>
                       <th>" . "Floor Closed" . "</th>
                       <th>" . "Floor Status" . "</th>
                   </tr>";

        // Get all parking lot floors
        $floors = Parking_Lot_Floor::getAllFloorsForLotId($lotId);

        // Iterate through all
        foreach ($floors as $floor) {
            // Add the floor
            $table .= self::getParkingLotFloorRow($floor);
        }

        if ($_SESSION["user_type"] == 1) {
            // Add the new parking lot floor inputs row
            $table .= self::getParkingLotFloorInputRows($lotId);
        }

        // Close the table
        $table .= "</table>";

        // Return it
        return $table;
    }

    private static function getParkingLotFloorInputRows($lotId) {
        // Create the row
        $row = "<tr data-lot_id='$lotId'>";

        // Add the permit select box
        $row .= "<td>" . self::getPermitSelectBox() . "</td>";

        // Add the floor capacity input
        $row .= "<td><input type='text'></td>";

        // Add the floor handicap capacity input
        $row .= "<td><input type='text'></td>";

        // Add the floor closed input
        $row .= "<td><input type='checkbox'></td>";

        // Add the status
        $row .= "<td><img class='status' src='images/not_active.png'/></td>";

        // Close the first row and begin the second
        $row .= "</tr><tr>";

        // Add the submit td with a hidden error message
        $row .= "<td colspan='5'><div class='error_message'></div><br><button class='create_button' onclick='createNewFloor(this);'>+ New Floor</button></td>";

        // Close this row and the inner table
        $row .= "</tr>";

        // Return it
        return $row;
    }

    private static function getPermitSelectBox() {
        // Create the select box
        $select = "<select>";

        // Load the corresponding table
        $sqler = new SQLer();
        $sqler->sendQuery("Select * from parking_permits");
        while ($row = $sqler->getRow()) {
            // Add a corresponding option in the select
            $select .= "<option value=" . $row["permit_id"] . ">" . $row["permit_name"] . "</option>";
        }

        // Close the select
        $select .= "</select>";

        // Return it
        return $select;
    }


    public static function getParkingLotFloorRow($floor) {
        // Create the row
        $row = "<tr data-floor_id='$floor->id'>";

        // Get the corresponding permit
        $permitName = $floor->getPermitName();

        // Add the permit
        $row .= "<td>$permitName</td>";

        // Add the floor capacity
        $row .= "<td>$floor->availableSpots</td>";

        // Add the floor handicap capacity
        $row .= "<td>$floor->availableHandicapSpots</td>";

        // Add the floor closed input
        if ($floor->closed) {
            $row .= "<td><input type='checkbox' onchange='respondToFloorCloseCheck(this);' checked></td>";
        }
        else {
            $row .= "<td><input type='checkbox' onchange='respondToFloorCloseCheck(this);'></td>";
        }

        // Add the status
        $row .= "<td>" . self::getFloorStatusIcon($floor) . "</td>";

        // Close the row
        $row .= "</tr>";

        // Return it
        return $row;
    }
}
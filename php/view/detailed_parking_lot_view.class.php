<?php

/**
 * Created by PhpStorm.
 * User: drew
 * Date: 2/23/17
 * Time: 9:27 PM
 */

include_once(dirname(__DIR__) . "/parking_lot/parking_lot.class.php");

class Detailed_Parking_Lot_View
{
    protected $lot;
    protected $excludeReturnButton;

    public function __construct($lotId, $excludeReturnButton = FALSE) {
        // Get lot from lot id
        $this->lot = Parking_Lot::getByID($lotId);
        // Store whether should give return button
        $this->excludeReturnButton = $excludeReturnButton;
    }

    public function __toString() {
        // Create the container
        $container = "<div class='detailed_parking_lot' data-lot_id='" . $this->lot->id . "'</div>";

        // Add the photo
        if (strlen($this->lot->pic) > 0) {
            $container .= "<img class='lot_photo' src='uploads/" . $this->lot->pic . "'/>";
        }
        else {
            $container .= "<img class='lot_photo' src='images/no_media.png'/>";
        }

        // Create inner container
        $innerContainer = "<div class='detailed_parking_info'>";

        // Add the lot name
        $innerContainer .= "<h3>" . $this->lot->title . "</h3><br>";

        // Add the lot description
        $innerContainer .= "<span style='font-size: 10pt;'>" . $this->lot->description . "</span><br><br>";

        // Check for floors
        if (sizeof($this->lot->floors) > 0) {
            // Create the floors table (wrap it for scrolling)
            $floorsTable = "<div class='floor_scroll'><table>";

            // Add the headers
            $floorsTable .= "<tr><th>Floor</th><th>Availability</th><th>Handicap Availability</th><th>Permit Requirements</th></tr>";

            // Create the floor count
            $floorCount = 1;

            // Iterate through all of the lot floors
            foreach ($this->lot->floors as $floor) {
                // Create a new row
                $row = "<tr>";

                // Add the title cell
                $row .= "<td>$floorCount</td>";

                // Create total style
                $totalStyle = "";

                // If no availabiliies
                if ($floor->availableSpots == 0) {
                    $totalStyle .= "background-color: red";
                }
                else if ($floor->availableSpots/$floor->totalSpots < 0.3) {
                    $totalStyle .= "background-color: orange";
                }
                else {
                    $totalStyle .= "background-color: green";
                }

                // Add the availabilities cell
                $row .= "<td style='$totalStyle'>" . $floor->availableSpots . "/" . $floor->totalSpots . "</td>";

                // Create total handicap style
                $totalHandicapStyle = "";

                // If no availabiliies
                if ($floor->availableHandicapSpots == 0) {
                    $totalHandicapStyle .= "background-color: red";
                }
                else if ($floor->availableHandicapSpots/$floor->totalHandicapSpots < 0.3) {
                    $totalHandicapStyle .= "background-color: orange";
                }
                else {
                    $totalHandicapStyle .= "background-color: green";
                }

                // Add the handicap availabilities cell
                $row .= "<td style='$totalHandicapStyle'>" . $floor->availableHandicapSpots . "/" . $floor->totalHandicapSpots . "</td>";

                // Add the permit cell
                $row .= "<td>" . $floor->getPermitName() . "</td>";

                // Close the row
                $row .= "</tr>";

                // Add the row to the table
                $floorsTable .= $row;

                // Increment the floor count
                $floorCount++;
            }

            // Close the table
            $floorsTable .= "</table></div>";

            // Add the table to the inner container
            $innerContainer .= $floorsTable;
        }
        else {
            // Inform of no floors
            $innerContainer .= "Currently, there is no floor data for this parking lot.";
        }

        // Close the inner container
        $innerContainer .= "</div>";

        // Add the inner container to the outer
        $container .= $innerContainer;

        // Close the container
        $container .= "</div>";

        // Create the return button
        $return = "<div id='return_button' onclick='returnToParkingLotList();'>Return</div>";

        // If skip return
        if ($this->excludeReturnButton) {
            $return = "";
        }

        // Return the string
        return $container . $return;
    }

    // Give the latest lot view (without the return button)
    public static function getDetailedLotView($lotId) {
        return (new self($lotId, TRUE))->__toString();
    }
}
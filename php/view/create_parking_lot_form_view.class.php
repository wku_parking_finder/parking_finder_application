<?php

/**
 * Created by PhpStorm.
 * User: drew
 * Date: 3/21/17
 * Time: 9:53 AM
 */
class Create_Parking_Lot_Form_View
{
    public function __toString() {
        // Return the container
        return $this->getClickout() . $this->getContainer();
    }

    protected function getClickout() {
        // Return the clickout
        return "<div id='create_parking_lot_clickout' onclick='closeCreateParkingLotPopup();'></div>";
    }

    protected function getContainer() {
        // Create the container
        $container = "<div id='create_parking_lot_container'>";

        // Create the container header
        $container .= "<h3>Please Provide the General Parking Lot Data</h3><br><br>";

        // Create the name input and label
        $container .= "<label>Parking Lot Name: </label>";
        $container .= "<input type='text' name='lot_name' placeholder='Lot Name' maxlength='30'>";
        $container .= "<br><br>";

        // Create the description input and label
        $container .= "<label>Parking Lot Description: </label>";
        $container .= "<textarea name='lot_description' placeholder='Lot Description' maxlength='255'></textarea>";
        $container .= "<br><br><hr><br>";

        // Create the image input and label and upload button
        $container .= "<label>Parking Lot Image: </label>
                        <input type='file' name='lot_photo'/> 
                        <button id='uploadLotPhotoButton' onclick='uploadLotPhoto();'>Upload</button>
                        <br>";

        // Create the preview image
        $container .= "<img class='lot_photo_upload' src='images/no_media.png'><br><hr><br>";

        // Add the form validation notifier
        $container .= "<span class='error_message'></span><br>";

        // Create the submit button
        $container .= "<button onclick='createNewParkingLot();' class='create_button'>Create</button>";

        // Close it and return it
        return $container . "</div>";
    }
}
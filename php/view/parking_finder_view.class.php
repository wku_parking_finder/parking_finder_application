<?php

/**
 * Created by PhpStorm.
 * User: ant45527
 * Date: 2/13/2017
 * Time: 9:46 AM
 */

class Parking_Finder_View
{
    public function __toString() {
        // Return the components as a string
        return $this->getBackdrop() . $this->getBanner(). $this->getContainer() . $this->getFooter();
    }

    protected function getBackdrop() {
        // Create the backdrop
        $backdrop = "<div id='backdrop'><img src='images/wku_background.jpg'></div>";

        // Return it
        return $backdrop;
    }

    protected function getBanner() {
        // Create the banner
        $banner = "<div id='banner'>
                    <img id='wku_logo' src='images/wku_logo.png'>
                    <img id='parking_finder_logo' src='images/parking_finder_logo_banner.png'>
                    <img id='settings_button' onclick='toggleLoginForm();' src='images/cog.png'>" .
                    $this->getLoginForm()
                    . "</div>";

        // Return it
        return $banner;
    }

    protected function getLoginForm() {
        // Create the container
        $container = "<div id='login_form'>";

        // Add a title to the container
        $container .= "<h3>Login to the Admin Control Panel</h3>";

        // Add the login label and input
        $container .= "<label>Username: </label>";
        $container .= "<input type='text' name='username'><br>";

        // Add the password label and input
        $container .= "<label>Password: </label>";
        $container .= "<input type='password' name='password'><br>";

        // Add the error message container
        $container .= "<div class='error_message'></div>";

        // Add the submit
        $container .= "<button onclick='login();'>Login</button>";

        // Return it
        return $container;
    }

    protected function getContainer() {
        // Start the session
        session_start();

        // Create the sorting inputs
        $sortingContainer = "<div id='parking_lot_list_sorting'>";
        $sortingContainer .= "<label>Sort By: </label>";
        $sortingContainer .= "<select id='sortBySelect' onchange='sortLotsBy(this);'>";

        // Check for the current sorting
        if (isset($_SESSION["sorting"]) && !is_null($_SESSION["sorting"])) {
            if ($_SESSION["sorting"] == "availability") {
                $sortingContainer .= "<option value='name'>Name (Alphabetical)</option>
                                     <option value='availability' selected>Availability</option>  
                                       <option value='closings'>Closings</option>";
            }
            else if ($_SESSION["sorting"] == "closings") {
                $sortingContainer .= "<option value='name'>Name (Alphabetical)</option>
                                     <option value='availability'>Availability</option>  
                                       <option value='closings' selected>Closings</option>";
            }
            else if ($_SESSION["sorting"] == "name") {
                $sortingContainer .= "<option value='name' selected>Name (Alphabetical)</option>
                                     <option value='availability'>Availability</option>  
                                       <option value='closings'>Closings</option>";
            }
        }
        else {
            $sortingContainer .= "<option value='name'>Name (Alphabetical)</option>
                                     <option value='availability'>Availability</option>  
                                       <option value='closings'>Closings</option>";
        }

        // Close the select
        $sortingContainer .= "<select>";

        // Add the filter options
        $sortingContainer .= "<label style='margin-left:40px;'>Filter: </label>";
        // Determine if should be checked
        $checked = (isset($_SESSION["filtering_closed"]) && $_SESSION["filtering_closed"] == 1 ? "checked" : "");
        // Add the checkbox option for closed
        $sortingContainer .= "<span class='filter_option'>Closed<input value='closed' onchange='updateFiltering(this);' type='checkbox' " . $checked . "/></span>";

        $sortingContainer .= "</div>";

        // Create the container
        $lotListContainer = self::getLotsTable();

        // Return the container
        return $sortingContainer . $lotListContainer;
    }

    protected function getFooter() {
        // Create the footer
        $footer = "<div id='footer'><span>&copy Copyright Dao, Netthisinghe, Norman, and Renaud 2017</span></div>";

        // Return it
        return $footer;
    }

    public static function getLotsTable() {
        // Create the container
        $container = "<div id='parking_lot_list'>";

        // Get the parking lots
        $parkingLots = Parking_Lot::getAllLots(TRUE);

        // Add the parking lots to the container
        $container .= implode($parkingLots);

        // Close the container
        $container .= "</div>";

        // Return
        return $container;
    }
}
<?php

/**
 * Created by PhpStorm.
 * User: drew
 * Date: 2/15/17
 * Time: 9:30 AM
 */

include_once(dirname(__DIR__) . "/sqler/sqler.class.php");

class Change_Password
{

    public $changePassword;

    // Creates the lot
    public function __construct($changePassword) {
        // Set the instance variables
        $this->changePassword = $changePassword;

    }

    // Saves the object to the database
    public function save() {
        // Create an sqler1
        $sqler = new SQLer();
        
            $userType = $_SESSION["user_type"];
            // So perform change password
            $sqler->sendQuery("UPDATE users
                                  SET password='$this->changePassword'
                                  WHERE user_type='$userType'");
            // Return if row count is greater than zero
            return true;
    }
}
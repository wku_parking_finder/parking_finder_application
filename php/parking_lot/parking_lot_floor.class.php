<?php

/**
 * Created by PhpStorm.
 * User: drew
 * Date: 3/29/17
 * Time: 12:06 PM
 */
class Parking_Lot_Floor
{
    protected static $tableName = "parking_lot_floors";

    public $id;
    public $lotId;
    public $permitId;
    public $closed;
    public $totalSpots;
    public $totalHandicapSpots;
    public $availableSpots;
    public $availableHandicapSpots;
    public $lastUpdated;
    public $status;

    // Creates the lot
    public function __construct($lotId, $permitId, $totalSpots, $totalHandicapSpots,
                                $closed = FALSE, $availableSpots = 0,
                                $availableHandicapSpots = 0, $lastUpdated = "",
                                $id = 0, $status = 1) {
        // Set the instance variables
        $this->lotId = $lotId;
        $this->permitId = $permitId;
        $this->totalSpots = $totalSpots;
        $this->totalHandicapSpots = $totalHandicapSpots;
        $this->availableSpots = $availableSpots;
        $this->availableHandicapSpots = $availableHandicapSpots;
        $this->closed = $closed;
        $this->lastUpdated = $lastUpdated;
        $this->id = $id;
        $this->status = $status;
    }

    public function getPermitName() {
        // Create the SQLer
        $sqler = new SQLer();

        // Select the permit matching this lot's permit id
        $sqler->sendQuery("Select permit_name from parking_permits where permit_id=$this->permitId");

        // Get the result
        $row = $sqler->getRow();

        // Create name variable
        $name = "";

        // Ensure good result
        if ($row) {
            return $row["permit_name"];
        }

        // Return the name
        return $name;
    }

    // Saves the object to the database
    public function save() {
        // Create an sqler
        $sqler = new SQLer();

        // Check if there is already a record
        if ($this->id == 0) {
            // So perform insert
            if(!$stmt = $sqler->con->prepare("INSERT INTO parking_lot_floors 
                                            (lot_id, permit_id, closed, total_spots, total_handicap_spots, 
                                            available_spots, available_handicap_spots, display_status) 
                                             VALUES (?,?,?,?,?,?,?,?)"))
            {
                echo "Prepare fail (" . $sqler->con->errno . ") " . $sqler->con->error;
            }
            if(!$stmt->bind_param("iiiiiiii", $this->lotId, $this->permitId, $this->closed, $this->totalSpots,
                                $this->totalHandicapSpots, $this->availableSpots, $this->availableHandicapSpots, $this->status))
            {
                echo "Bind fail (" . $stmt->errno . ") " . $stmt->error;
            }
            if($stmt->execute())
            {
                return TRUE;
            }
            else
            {
                $error = "Execute fail (" . $stmt->errno . ") " . $stmt->error; // Print the error
                $stmt->close();
                echo $error;
            }
        }
        else {
            // So perform update
            $sqler->sendQuery("UPDATE parking_lot_floors
                                  SET lot_id=$this->lotId,
                                      permit_id=$this->permitId,
                                      closed=$this->closed,
                                      total_spots=$this->totalSpots,
                                      total_handicap_spots=$this->totalHandicapSpots,
                                      available_spots=$this->availableSpots,
                                      available_handicap_spots=$this->availableHandicapSpots,
                                      display_status=$this->status
                                  WHERE floor_id=$this->id");
            // Return if row count is greater than zero
            return TRUE;
        }

        // Return false since no success has been returned yet
        return FALSE;
    }

    // Returns an object loaded from the database
    public static function getByID($id) {
        // Load the corresponding table
        $sqler = new SQLer();
        $sqler->sendQuery("Select * from " . self::$tableName . " where floor_id=$id");
        if ($row = $sqler->getRow()) {
            // Add a new lot for each record in the database
            return new self($row["lot_id"], $row["permit_id"], $row["total_spots"],
                            $row["total_handicap_spots"], $row["closed"],
                            $row["available_spots"], $row["available_handicap_spots"], $row["last_update"],
                            $id, $row["display_status"]);
        }
        return NULL;
    }

    // Gets all floors for the given lot id
    public static function getAllFloorsForLotId($lotId) {
        // Create the array to return
        $floors = [];

        // Load the corresponding table
        $sqler = new SQLer();
        $sqler->sendQuery("Select floor_id from " . self::$tableName . " where lot_id=$lotId");
        while ($row = $sqler->getRow()) {
            // Add the floor to the array to return
            $floors[] = self::getByID($row["floor_id"]);
        }

        // Return it
        return $floors;
    }
}
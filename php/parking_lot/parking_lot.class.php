<?php

/**
 * Created by PhpStorm.
 * User: drew
 * Date: 2/15/17
 * Time: 9:30 AM
 */

include_once(dirname(__DIR__) . "/sqler/sqler.class.php");
include_once(dirname(__DIR__) . "/parking_lot/parking_lot_floor.class.php");

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

class Parking_Lot
{
    protected static $tableName = "parking_lots";

    public $id;
    public $title;
    public $description;
    public $pic;
    public $floors;
    public $status;
    public $closed;

    // Creates the lot
    public function __construct($title, $description,
                                $pic, $id = 0, $status = 1, $closed = 0) {
        // Set the instance variables
        $this->id = $id;
        $this->title = $title;
        $this->description = $description;
        $this->pic = $pic;
        $this->status = $status;
        $this->closed = $closed;

        // Get the floors
        $this->floors = $this->getFloors();
    }

    protected function getFloors() {
        // No floors since not in the database yet
        if ($this->id == 0) {
            return [];
        }

        // Create the array to reutrn
        $floors = [];

        // Create the sqler
        $sqler = new SQLer();

        // Send the query
        $sqler->sendQuery("SELECT * from parking_lot_floors where lot_id=$this->id");

        // Process the results
        while ($row = $sqler->getRow()) {
            // Create the new floor from the data
            $floors[] = new Parking_Lot_Floor($row["lot_id"], $row["permit_id"], $row["total_spots"],
                $row["total_handicap_spots"], $row["closed"], $row["available_spots"],
                $row["available_handicap_spots"], $row["last_update"], $row["floor_id"], $row["display_status"]);
        }

        // Return the floors
        return $floors;
    }

    // Closes all floors for a lot
    public function toggleAllFloorClosings($close) {
        // Iterate through all floors
        foreach ($this->floors as $floor) {
            // Update the close status
            $floor->closed = $close;
            // Save the floor
            $floor->save();
        }
    }

    // Set all floors to active or inactive
    public function toggleAllFloorStatuses($status) {
        // Iterate through all floors
        foreach ($this->floors as $floor) {
            // Update the close status
            $floor->status = $status;
            // Save the floor
            $floor->save();
        }
    }

    // Saves the object to the database
    public function save() {
        // Create an sqler
        $sqler = new SQLer();

        // Check if there is already a record
        if ($this->id == 0) {
            // So perform insert
            if(!$stmt = $sqler->con->prepare("INSERT INTO parking_lots 
                                            (lot_name, lot_desc, lot_pic, display_status, closed) 
                                             VALUES (?,?,?,?,?)"))
            {
                echo "Prepare fail (" . $sqler->con->errno . ") " . $sqler->con->error;
            }
            if(!$stmt->bind_param("sssii", $this->title, $this->description, $this->pic, $this->status, $this->closed))
            {
                echo "Bind fail (" . $stmt->errno . ") " . $stmt->error;
            }
            if($stmt->execute())
            {
                return TRUE;
            }
            else
            {
                $error = "Execute fail (" . $stmt->errno . ") " . $stmt->error; // Print the error
                $stmt->close();
                echo $error;
            }
        }
        else {
            // So perform update
            $sqler->sendQuery("UPDATE parking_lots 
                                  SET lot_name='$this->title',
                                      lot_desc='$this->description',
                                      lot_pic='$this->pic',
                                      display_status=$this->status,
                                      closed=$this->closed
                                  WHERE lot_id=$this->id");
            // Return if row count is greater than zero
            return TRUE;
        }

        // Return false since no success has been returned yet
        return FALSE;
    }

    // Returns a string representation of the total availability (as a fraction)
    private function getAvailabilitySpans() {
        // Track the capacity
        $capacity = 0;
        // Track the availabilities
        $availability = 0;
        // Track the handicap capacity
        $handicapCapacity = 0;
        // Track the handicap availabilities
        $handicapAvailability = 0;
        // Add up all floor availabilities
        foreach ($this->floors as $floor) {
            // Add to the total capacity
            $capacity += $floor->totalSpots;
            // Add to the total availability
            $availability += $floor->availableSpots;
            // Add to the handicap capacity
            $handicapCapacity += $floor->totalHandicapSpots;
            // Add to the handicap availability
            $handicapAvailability += $floor->availableHandicapSpots;
        }

        // Create total style variable
        $totalStyle = "padding: 5px; border-radius: 10px;";

        // Style as full
        if ($availability == 0) {
            $totalStyle .= "background-color: red;";
        }
        // Style as nearly full
        else if ($availability/$capacity < 0.3) {
            $totalStyle .= "background-color: orange;";
        }
        // Style as full
        else {
            $totalStyle .= "background-color: green;";
        }

        // Create handicap style variable
        $totalHandicapStyle = "padding: 5px; border-radius: 10px;";

        // Style as full
        if ($handicapAvailability == 0) {
            $totalHandicapStyle .= "background-color: red;";
        }
        // Style as nearly full
        else if ($handicapAvailability/$handicapCapacity < 0.3) {
            $totalHandicapStyle .= "background-color: orange;";
        }
        // Style as full
        else {
            $totalHandicapStyle .= "background-color: green;";
        }

        // Return the formatted string
        return "<span style='$totalStyle'>Availability: " . $availability . " / " . $capacity . "</span><br><br>"
                . "<span style='$totalHandicapStyle'>Handicap Availability: " . $handicapAvailability . " / " . $handicapCapacity . "</span>";
    }

    // Prints the HTML
    public function __toString()
    {
        // Build the container
        $container = "<div class='parking_lot' data-lot_id='$this->id' onclick='showDetailedLotView(this);'>";

        // Check if inactive
        if ($this->status == 0) {
            return "";
        }

        // Check if closed
        if ($this->closed == 1) {
            $container = "<div class='parking_lot closed' data-lot_id='$this->id' onclick='showDetailedLotView(this);'>";
        }

        // Add the photo
        if (strlen($this->pic) > 0) {
            $container .= "<img class='inline_lot_photo' src='uploads/" . $this->pic . "'/>";
        }
        else {
            $container .= "<img class='inline_lot_photo' src='images/no_media.png'/>";
        }

        if ($this->closed == 1) {
            // Add the title with indicator of closing
            $container .= "<h2>$this->title [CLOSED]</h2>";
        }
        else {
            // Add the title
            $container .= "<h2>$this->title</h2>";
        }

        // Create an inner container
        $innerContainer = "<div>";

        // Add the availability data
        $innerContainer .= $this->getAvailabilitySpans() . "<br><br>";

        // Close the inner container
        $innerContainer .= "</div>";

        // Add the inner container to the outer
        $container .= $innerContainer;

        // Close the container
        $container .= "</div>";

        // Return
        return $container;
    }

    // Gets all of the lots available
    public static function getAllLots($useSessionOrdering = TRUE) {
        // Create the array to hold all of the lots
        $lots = [];

        // Create where string for query
        $where = "";

        // Set where
        if (isset($_SESSION["filtering_closed"]) && $_SESSION["filtering_closed"] == 1) {
            $where = "where parking_lots.closed = 0";
        }

            // Create the query
        $query = "Select lot_id from parking_lots $where";
        if ($useSessionOrdering && isset($_SESSION["sorting"])) {
            if ($_SESSION["sorting"] == "closings") {
                $query = "Select parking_lots.lot_id from parking_lots " . $where .
                            " order by closed ASC";
            }
            else if ($_SESSION["sorting"] == "availability") {
                $query = "Select parking_lots.lot_id from parking_lots
                            left join parking_lot_floors 
                            on parking_lots.lot_id = parking_lot_floors.lot_id " . $where .
                            " group by parking_lots.lot_id, parking_lot_floors.available_spots
                            order by parking_lots.closed ASC,
                            parking_lot_floors.available_spots DESC";
            }
            else if ($_SESSION["sorting"] == "name") {
                $query = "Select parking_lots.lot_id, parking_lots.lot_name from parking_lots " . $where .
                            " order by lot_name ASC";
            }
        }

        // Load the lots from the lot table
        $sqler = new SQLer();
        $sqler->sendQuery($query);
        // Track ids
        $ids = [];
        while ($row = $sqler->getRow()) {
            // If visited, skip
            if (in_array($row["lot_id"], $ids)) {
                continue;
            }
            // Add a new lot for each record in the database
            $lots[] = self::getByID($row["lot_id"]);

            // Track visited
            $ids[] = $row["lot_id"];
        }

        // Return
        return $lots;
    }

    // Returns an object loaded from the database
    public static function getByID($id) {
        // Load the corresponding table
        $sqler = new SQLer();
        $sqler->sendQuery("Select * from " . self::$tableName . " where lot_id=$id");
        if ($row = $sqler->getRow()) {
            // Add a new lot for each record in the database
            return new self($row["lot_name"], $row["lot_desc"], $row["lot_pic"], $id, $row["display_status"], $row["closed"]);
        }
        return NULL;
    }
}